# 自动部署工具

## 初始化

###  安装依赖

> pip install gitpython requests colorama

### 新建配置文件

> vi deploy.ini

添加以下内容

    [global]
    host = https://[yourgitlab.host]
    token = deploer gitlab user token
    
或者通过交互方式初始化

> auto_deploy --init 

 
host 是gitlab地址

token  在 https://[yourgitlab.host]/profile/personal_access_tokens 可获取到

### 添加站点

执行下面命令按照提示 输入信息

> auto_deploy.py -A

    input sitename, must unique
    > ksc-testsite
    input remote project name, e.g. [useranme]/[project_name]
    > ksc/test
    input local repo directory path(absolute path)
    > d:/test/ksc-testsite
    input branch (master default)
    > develop
    config info:
            [git-ksc-testsite]
            name = ksc/test
            local = d:/test/ksc-testsite
            branch = develop


### NOTICE

在window 或者 docker 环境下 需要指定 sh_composer

window:
> composer.bat  install --no-progress

docker:

> docker exec -it [contianer_name] composer  install --no-progress
